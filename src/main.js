import Router from './Router';
import GameList from './pages/GameList';
import PizzaForm from './pages/PizzaForm';
import FavoriteList from './pages/FavoriteList';

const accueil = new GameList(),
	favPage = new FavoriteList(),
	pizzaForm = new PizzaForm();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');
Router.routes = [
	{ path: '/', page: accueil, title: 'Liste des jeux' },
	{ path: '/favoris', page: favPage, title: 'Favoris' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

document.querySelector(
	'.logo'
).innerHTML += `<small>les pizzas c'est la vie</small>`;

// détection des boutons précédent/suivant du navigateur :
// on lit l'url courante dans la barre d'adresse et on l'envoie au Router
window.onpopstate = () => Router.navigate(document.location.pathname, false);
// affichage de la page initiale :
// même traitement que lors de l'appui sur les boutons précédent/suivant
window.onpopstate();

const searchButton = document.querySelector('.searchButton');
searchButton.addEventListener('click', event => {
	event.preventDefault();
	accueil.request(document.querySelector(`input[name="name"]`)?.value);
});

let triValue = '';
let triGenre = '';

fetch('https://api.rawg.io/api/genres')
	.then(response => response.json())
	.then(data => {
		let html = '';
		data.results.forEach(element => {
			html += `<option value="${element.id}">${element.name}</option>`;
		});
		document.getElementById('selectGenre').innerHTML += html;
	})
	.finally(() => {
		document.getElementById('selectGenre').addEventListener('click', event => {
			event.preventDefault();
			requestGenre(document.getElementById('selectGenre').value);
		});
	});

function requestGenre(value) {
	triGenre = `&genres=${value}`;
	if (value == 0) triGenre = '';
	accueil.request(triValue + triGenre);
	console.log(triValue + triGenre);
}

document.getElementById('boutonPertinence').addEventListener('click', event => {
	event.preventDefault();
	triValue = '';
	accueil.request(triValue + triGenre);
});
document.getElementById('boutonRecent').addEventListener('click', event => {
	event.preventDefault();
	triValue = '&ordering=released';
	accueil.request(triValue + triGenre);
});
document.getElementById('boutonNote').addEventListener('click', event => {
	event.preventDefault();
	triValue = '&ordering=-metacritic';
	accueil.request(triValue + triGenre);
});
