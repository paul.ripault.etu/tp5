export default class Loader {
	static loader = document.getElementById('loading');

	static afficherLoader() {
		this.loader.classList.add('visible');
	}

	static cacherLoader() {
		this.loader.classList.remove('visible');
	}
}
