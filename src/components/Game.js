import Component from './Component.js';
import Img from './Img.js';

export default class Game extends Component {
	constructor({ name, slug, background_image, metacritic }) {
		super('article', { name: 'class', value: 'pizzaThumbnail' }, [
			new Component('a', { name: 'id', value: slug }, [
				new Img(background_image),
				new Component('section', null, [
					new Component('h4', null, name),
					new Component('ul', null, [
						new Component('li', null, `Note Metacritic : ${metacritic}`),
						new Component('button', null, `Ajouter aux favoris`),
					]),
				]),
			]),
		]);
	}
}
