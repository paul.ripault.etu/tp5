import Page from './Page';
import Game from '../components/Game';

export default class GamePage extends Page {
	gameName;

	constructor(gameName) {
		super('pizzaList', gameName);
		this.gameName = gameName;
	}

	mount(element) {
		super.mount(element);
		this.request();
	}

	request() {
		const loader = document.getElementById('loading');
		loader.classList.add('visible');
		fetch(
			`https://api.rawg.io/api/games/${this.gameName}` // faut l'id à la place
		)
			.then(response => response.json())
			.then(
				data =>{
					console.log(data);
					(this.element.innerHTML = this.renderGameDetails(data))
				}
					
			).catch(error => console.log(error));
	}

	renderGameDetails(value) {
		const loader = document.getElementById('loading');
		loader.classList.remove('visible');
		console.log( new Game(value).render());
		return new Game(value).render();
	}
}
