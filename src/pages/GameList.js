import Page from './Page';
import Game from '../components/Game';
import Router from '../Router';
import FavoriteList from './FavoriteList';
import Loader from '../misc/Loader';

export default class GameList extends Page {
	constructor() {
		super('pizzaList'); // on passe juste la classe CSS souhaitée
	}

	mount(element) {
		super.mount(element);
		this.request('');
	}

	request(value) {
		Loader.afficherLoader();
		fetch(
			`https://api.rawg.io/api/games?dates=2020-01-01,2021-12-31&metacritic=50,100&search=${value}&page_size=60`
		)
			.then(response => response.json())
			.then(data => (this.element.innerHTML = this.renderGames(data.results))) // affichage dans la page
			.finally(() => {
				document.querySelectorAll('.pizzaThumbnail').forEach(thumbnail => {
					thumbnail.querySelector('button').addEventListener('click', event => {
						event.preventDefault();
						FavoriteList.add(thumbnail);
					});

					thumbnail.addEventListener('click', event => {
						event.preventDefault();
						this.navToGameDetails(thumbnail.querySelector('a').id);
					});
				});
			});
	}

	navToGameDetails(gameName) {
		console.log(gameName);
		Router.appendPage(gameName);
	}

	renderGames(value) {
		Loader.cacherLoader();
		let html = ``;
		value.forEach(element => {
			html += new Game(element).render();
		});
		//console.log(html)
		return html;
	}
}
