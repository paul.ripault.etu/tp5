import Game from '../components/Game';
import Page from './Page';

export default class FavoriteList extends Page {
	static liste = [];
	constructor(liste) {
		super('pizzaList');
	}

	mount(element) {
		super.mount(element);
		this.renderFavorite();
	}

	static add(game) {
		console.log(game);
		FavoriteList.liste.push(game);
	}
	static remove(game) {
		FavoriteList.liste.pop(game);
	}
	renderFavorite() {
		let html = ``;
		FavoriteList.liste.forEach(element => {
			html += `<article class="pizzaThumbnail"> ${element.innerHTML} </article>`;
			console.log(element);
		});
		this.element.innerHTML = html;
	}
}
